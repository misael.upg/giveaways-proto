# Get Windows colors, type ramp, and motion library out of the box
# For more info see https://github.com/Microsoft/windows-framer-toolkit
{SystemColor} = require 'SystemColor'
{Type} = require 'type'
motion = require 'motionCurves'

# If you want the purple outlines, comment this out
Framer.Extras.Hints.disable()

# # layerA = new Layer
# #     size: Screen.size
 
# # # Create FlowComponent 
# # flow = new FlowComponent
 
# # # Show the layer 
# # flow.showNext(layerA, animate: true)

# # Store categories
# categories = ['All', 'Fashion', 'Events', 'Gadgets', 'Fun', 'Food']

# fashionGradient = new Gradient
#     start: '#30cfd0'
#     end: '#330867'
# allGradient = new Gradient
#     start: '#9795f0'
#     end: '#fbc8d4'

# # Create a new PageComponent and only allow horizontal scrolling. 
# page = new PageComponent
#     width: Screen.width
#     height: Screen.height
#     scrollVertical: false
#     gradient: allGradient

# pageTitle = new TextLayer
#     text: categories[1]
#     parent: filterButton
#     fontFamily: 'Roboto'
#     color: 'white'
#     x: Align.center()
#     y: 30
#     height: 50
#     width: Screen.width
#     textAlign: 'center'

# topNavContainer = new ScrollComponent
#     x: Align.center()
#     y: pageTitle.maxY
#     width: Screen.width - 50
#     height: 160
#     scrollVertical: false
#     scrollHorizontal: true
#     backgroundColor: 'rgba(0,0,0,0)'
#     directionLock: yes

# topNavContainer.contentInset =
#     top: 30
#     right: 0
#     bottom: 30
#     left: 0

# # Store filters properties
# filterButtonW = 250
# filterButtonH = 100
# filterButonPadding = 10

# # Display categories inside container
# for i in [0...categories.length]
#     filterButton = new Layer
#         name: categories[i].toLowerCase() + 'Layer'
#         width: filterButtonW
#         height: filterButtonH
#         backgroundColor: 'white'
#         borderRadius: 6
#         parent: topNavContainer.content
#         x: (filterButtonW + filterButonPadding) * i  + filterButonPadding
#         shadowY: 4
#         shadowBlur: 20
#         shadowColor = 'rgba(0,0,0,0.2)'
#     filterName = new TextLayer
#         text: categories[i]
#         parent: filterButton
#         fontFamily: 'Roboto'
#         fontSize: 32
#         color: 'rgba(0,0,0,.9)'
#         x: Align.center()
#         y: Align.center()

# # Define the first page 
# pageOne = new Layer
#     id: 1
#     width: page.width
#     height: page.height
#     parent: page.content
#     backgroundColor: 'rgba(0,0,0,0)'
#     name: 'allLayer'

# pageTwo = new Layer
#     id: 2
#     width: page.width
#     height: page.height
#     backgroundColor: 'rgba(0,0,0,0)'
#     name: 'fashionLayer'

# pageThree = new Layer
#     id: 3
#     width: page.width
#     height: page.height
#     backgroundColor: 'rgba(0,0,0,0)'

# page.on "change:currentPage", ->
#     if page.currentPage.name == 'fashionLayer'
#         topNavContainer.content.childrenWithName(page.currentPage.name)[0].animate
#             scale: 1.1
#         page.animate
#             gradient:
#                 start: '#ebbba7'
#                 end: '#cfc7f8'
#     if page.currentPage.name == 'allLayer'
#         page.animate
#             gradient:
#                 start: '#9795f0'
#                 end: '#fbc8d4'
# # Crate cards for screen 1
# # for [0...categories.length]
# #     card = new Layer

# # layerA = new Layer
# #     x: Align.center()
# #     y: layerA.height + 60
# #     width: Screen.width - 50
# #     height: 550
# #     opacity: 1
# #     backgroundColor: "white"
# #     parent: pageOne
 
# #  layerA = new Layer
# #     x: Align.center()
# #     y: layerA.height + 60
# #     width: Screen.width - 50
# #     height: 550
# #     opacity: 1
# #     backgroundColor: "white"
# #     parent: pageTwo

# # layerA = new Layer
# #     x: Align.center()
# #     y: layerA.height + 60
# #     width: Screen.width - 50
# #     height: 550
# #     opacity: 1
# #     backgroundColor: "white"
# #     parent: pageThree

# # Add the second page to the right 
# page.addPage(pageTwo, "right")
# page.addPage(pageThree, "right")